output "gke_context_add_command" {
  value = "gcloud container clusters get-credentials ${var.gke_cluster_name} --region ${var.region}"
}

output "db_connection_proxy" {
  value = module.db.proxy_connection
}
