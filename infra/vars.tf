variable "project_id" {
  type = string
  default = "gcp-course-project-70107"
}

variable "region" {
  type = string
  default = "europe-west1"
}

variable "service_account_db_name" {
  type = string
  default = "service-account-db"
}

variable "service_account_cluster_name" {
  type = string
  default = "service-account-cluster"
}

variable "vpc_name" {
  type = string
  default = "vpc-gke"
}

variable "gke_cluster_name" {
  type = string
  default = "counter-cluster"
}

variable "db_name" {
  type = string
  default = "counter-db"
}

variable "vpc_subnet_cidr" {
  type = string
  default = "10.0.0.0/24"
}
