module cluster {
  source = "./cluster"
  vpc_id = google_compute_network.vpc_network.id
  vpc_subnet_id = google_compute_subnetwork.vpc_network_eu_west1_subnet.id
  region = var.region
  service_account_email = google_service_account.account_cluster.email
  gke_cluster_name = var.gke_cluster_name
}

module db {
  source = "./db"
  vpc_id = google_compute_network.vpc_network.id
  region = var.region
  db_name = var.db_name
  databases = ["counter"]
  depends_on = [google_service_networking_connection.private_vpc_connection]
}

module gke_secrets {
  source = "./gke_secrets"
  host = module.cluster.host_endpoint
  token = data.google_client_config.provider.access_token
  cluster_ca_certificate = module.cluster.cluster_ca_certificate
  db_username = module.db.db_user_name
  db_password = module.db.db_user_password
  service_account_db_private_key = google_service_account_key.account_db_service_key.private_key
}
