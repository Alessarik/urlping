const got = require('got');
const { Client } = require('pg');

console.log("START...");

function check(name, value) {
    if (value) {
        console.log(name, ":", value);
    } else {
        console.log(`Attention! Check ${name} environment variable!`);
        throw name;
    }
}

function create_table(client, table_name) {
    const query = `
        CREATE TABLE IF NOT EXISTS ${table_name} (
            "timestamp" BIGINT NOT NULL,
            "code" INTEGER NOT NULL,
            "latency" INTEGER NOT NULL
        );
    `;

    client.query(query, (error, response) => {
        if (error) {
            console.log("Error", error);
            return;
        }
        console.log("Response:", response.rowCount);
    });
}

function insert_into_table(client, table_name, timestamp, code, latency) {
    const query = `
        INSERT INTO ${table_name} (
            timestamp, code, latency
        ) values (
            ${timestamp}, ${code}, ${latency}
        )
    `;

    client.query(query, (error, response) => {
        if (error) {
            console.log("Error", error);
            client.end();
            return;
        }
        console.log("Response:", response.rowCount);
        client.end();
        console.log("...FINISH");
    });
}

const jitsuURL = 'https://t.jitsu.com/api/v1/s2s/event';

var NAME = process.env.NAME;
var URL = process.env.URL;
var HOST = process.env.HOST || 'localhost';
var PORT = process.env.PORT ? parseInt(process.env.PORT, 10) : 5432;
var USER = process.env.USER || 'postgres';
var PASS = process.env.PASS;
var DB = process.env.DB || 'counter';
var KEY = process.env.KEY;

check("NAME", NAME);
check("URL",  URL);
check("HOST", HOST);
check("PORT", PORT);
check("USER", USER);
check("PASS", !!PASS);
check("DB",   DB);
check("KEY",  !!KEY);

(async () => {
    try {
        var start = Date.now();
        const response = await got(URL);
        var delay = Date.now() - start;
        console.log(response.statusMessage, response.statusCode, delay);

        const client = new Client({
            host: HOST,
            port: PORT,
            database: DB,
            user: USER,
            password: PASS
        });

        await got.post(jitsuURL, {
            "headers": {
                "X-Auth-Token": KEY
            },
            "json": true,
            "body": {
                "name": NAME,
                "code": response.statusCode,
                "latency": delay
            }
        }).then((result) => {
            console.log("JITSU RESPONSE:", result.statusMessage, result.statusCode, result.body);
        })
        .catch((error) => {
            console.log("JITSU ERROR:", error.statusMessage, error.statusCode, error);
        });

        client.connect();
        create_table(client, NAME);
        insert_into_table(client, NAME, start, response.statusCode, delay);

    } catch(error) {
        console.log(error);
    };

})();

console.log("...WAIT...");
